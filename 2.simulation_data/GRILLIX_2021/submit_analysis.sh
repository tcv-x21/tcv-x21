#!/bin/bash
# Standard output and error (goes into a 'processing' folder which must exist in advance)
#SBATCH -o processing/%x.%A_%a.1x1x48.out
#SBATCH -e processing/%x.%A_%a.1x1x48.err
# Initial working directory:
#SBATCH -D ./
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=48
#
# Wall clock limit:
#SBATCH --time=02:00:00
# 
# MARCONI SPECIFIC
#SBATCH -p skl_fua_dbg     # partition: see sinfo (skl_fua_pro, skl_fua_dbg, knl_fua_prod)
#SBATCH -A FUA35_EST3D    # account: saldo -b --skl

module purge
module load profile/base
module load intel/pe-xe-2018--binary
module load intelmpi/2018--binary
module load mkl/2018--binary
module load zlib/1.2.8--gnu--6.1.0
module load szip/2.1--gnu--6.1.0
module load hdf5/1.10.4--intel--pe-xe-2018--binary
module load netcdf/4.6.1--intel--pe-xe-2018--binary
module load netcdff/4.4.4--intel--pe-xe-2018--binary

export I_MPI_PIN_MODE=lib
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=cores

echo $SLURM_JOB_NAME

~/.conda/envs/tcvx21/bin/python -u run_analysis.py $SLURM_JOB_NAME
# Contributing to this repository

## Requests for support/more information

While you can of course contact the authors directly to get support for using TCV-X21, we encourage you to use the
"issues" functionality in Gitlab. This means that your questions can help other users as well.

We'd be delighted to help you use the repository: let us know if anything can be made clearer, and we can extend the repository.

## Contributing new data

If you've gathered new data in the TCV-X21 scenario or you've validated a simulation of your own, excellent!

This repository is designed to be dynamic, so that we include new data.

To do this, push your data to a new branch and then submit a merge request to have your data included in the main repository.

## Making your own open dataset

We hope that you find this repository useful. If you do, and would like to also publish an open dataset, that's great news.
All of the software (i.e. everything that isn't data) is licenced under an MIT licence, so you can take what you need from
this repository. Also, we'd be happy to provide suggestions via Gitlab issues (and can make a Wiki if there's sufficient
interest). The MIT licence means that if you're only using the software, you can take bits and pieces without worrying about
"providing proper attribution" and "indicating if the dataset has been modified" (which are the terms of the CC-BY-4.0 licence
which applies to the data).

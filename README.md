# ⚠️ Warning: development version ⚠️

This repository is the pre-release development version of the TCV-X21 repository. You can find the permanent home of the repository at https://github.com/SPCData/TCV-X21

# TCV-X21 validation for divertor turbulence simulations

Welcome to TCV-X21. We're glad you've found us!

This repository is designed to let you perform the analysis presented in *Oliveira and Body et. al., Nuclear Fusion, 2021*, both using the data given in the paper, and with a turbulence simulation of your own. We hope that, by providing the analysis, the TCV-X21 case can be used as a standard validation and bench-marking case for turbulence simulations of the divertor in fusion experiments. The repository allows you to scrutinise and suggest improvements to the analysis (there's always room for improvement), to directly interact with and explore the data in greater depth than is possible in a paper, and — we hope — use this case to test a simulation of your own.

To use this repository, you'll need to either use the `mybinder.org` link below OR user rights on a computer with Python-3, conda and git-lfs pre-installed.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Ftcv-x21%2Ftcv-x21/HEAD?urlpath=lab/tree/tcv-x21.ipynb)

## Video tutorial

This quick tutorial shows you how to navigate the repository and use some of the functionality of the library.

![Tutorial](Tutorial_with_captions.mp4)

## What can you find in this repository

* `1.experimental_data`: data from the TCV experimental campaign, in NetCDF, MATLAB and IMAS formats, as well as information about the reference scenario, and the reference magnetic geometry (in `.eqdsk`, `IMAS` and `PARALLAX-nc` formats)
* `2.simulation_data`: data from simulations of the TCV-X21 case, in NetCDF format, as well as raw data files and conversion routines
* `3.results`: high resolution PNGs and LaTeX-ready tables for a paper
* `tcvx21`: a Python library of software, which includes
  * `record_c`: a class to interface with NetCDF/HDF5 formatted data files
  * `observable_c`: a class to interact with and plot observables
  * `file_io`: tools to interact with MATLAB and JSON files
  * `quant_validation`: routines to perform the quantitative validation
  * `analysis`: statistics, curve-fitting, bootstrap algorithms, contour finding
  * `units_m.py`: setting up `pint`-based unit-aware analysis (it's difficult to overstate how cool this library is)
  * `grillix_post`: a set of routines used for post-processing GRILLIX simulation data, which might help if you're trying to post-process your own simulation. You can see a worked example in `simulation_postprocessing.ipynb`
* `notebooks`: Jupyter notebooks, which allow us to provide code with outputs and comments together
  * `simulation_setup.ipynb`: what you might need to set up a simulation to test
  * `simulation_postprocessing.ipynb`: how to post-process the data
  * `data_exploration.ipynb`: some examples to get you started exploring the data
  * `bulk_process.ipynb`: runs over every observable to make the `results` — which you'll need to do if you're writing a paper from the results
* `tests`: tests to make sure that we haven't broken anything in the analysis routines
* `README.md`: this file, which helps you to get the software up and running, and to explain where you can find everything you need. It also provides the details of the licencing (below). There's more specific `README.md` files in several of the subfolders.

and lots more files. If you're not a developer, you can safely ignore these.

## What can't you find in this repository

Due to licencing issues, the source code of the simulations is *not provided*. Sorry!

Also, the raw simulations are not provided here due to space limitations (some runs have more than a terabyte of data), but they are all backed up on archive servers. If you'd like to access the raw data, get in contact.

## License and attribution notice

We distinguish between *data* (files containing binary data or non-executable plain text files) and *software* (Python scripts and infrastructure, excluding Jupyter notebooks since these also contain data). The TCV-X21 data is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a> (CC-BY 4.0). The associated software in the repository is licensed under a permissive MIT license.

The legal text of the CC-BY 4.0 license can be found at [Creative Commons](https://creativecommons.org/licenses/by/4.0/legalcode). A human-readable summary of (and not a replacement of) the CC-BY 4.0 license can be found at [Creative Commons](https://creativecommons.org/licenses/by/4.0/), which is interpreted here. If you're using the dataset in this repository personally, you can use it pretty much however you'd like. If you use the data in a scientific article, we ask that you cite the paper "*Oliveira and Body et. al., Nuclear Fusion, 2021*" (i.e. *provide suitable attribution*). If you want to share the dataset, make sure that you keep a link to the original CC-BY 4.0 license, clearly indicate if you've changed the dataset, and include a statement that we (the TCV-X21 team) do not endorse your use of the dataset (unless such endorsement has been specifically arranged with us). Finally, although not required by the license, we would appreciate it if you inform us how you're using the dataset, since this will encourage the release of future datasets.

Conversely, if you're *only* using the software in the repository (and none of the data), an MIT license applies. The text of this is available in [`LICENSE`](LICENSE), but it basically says "do whatever you want, don't sue us". The distinction between data and software is made so that you can copy-paste out the post-processing and testing infrastructure, without having to worry about the (slightly) more restrictive CC-BY 4.0 license which applies to the data.


# Running the Jupyter notebooks (installation as non-root user)

To run the Jupyter notebooks, you have two options. The first is to use the `mybinder.org` interface, which let you interact with the notebooks via a web interface. You can launch the binder for this repository by clicking the binder badge in the repository header. Note that not all of the
repository content is copied to the Docker image (this is specified in `.dockerignore`). The large checkpoint files are not
included in the image, although they can be found in the repository at `simulation_data/GRILLIX/checkpoints_for_1mm`.
Additionally, because we have removed a lot of the repository via `.dockerignore`, the default docker image will not work with
git.

If you'd like to run one of the other notebooks or to extend the repository, you'll need to *install* additional Python packages. First of all, you need Python-3 and conda installed (latest versions recommended). Then, to install the necessary packages, we make a sandbox environment. This has a few advantages to installing packages globally — sudo rights are not required, you can install package versions without risking breaking other Python scripts, and if everything goes terribly wrong you can easily delete everything and restart.

Unfortunately, this is the point where you're most likely to encounter issues. Apologies in advance, and raise an issue in Gitlab if you get stuck.

If you've got Python-3, conda and git-lfs you *should* be able to just run (in a terminal, from the same directory as this `README.md` file) either

### Option A: install script

This shell script performs the installation in one step (must be run from the root directory of `tcvx21` repository).
It will

1. clean the folder `env` if it already exists
2. install the packages listed in `environment.yml` into the `env` folder
3. install a Jupyter kernel `tcvx21` into your global Jupyter list of kernels

```shell
./install_env.sh env clean
```

The installation into the local folder `./env` has the advantage that, once you're finished testing, you can simply delete the
`tcvx21` folder and the environment will also be cleaned up. To remove the Jupyter kernel you might need to run
`jupyter kernelspec uninstall tcvx21`.

### Option B: install manually

If the above fails, or you (wisely) don't like running random shell scripts, you can also execute the following (again, must be run in the root of `tcvx21`)

```bash
conda env create --prefix ./env --file=environment.yml
conda activate ./env
pip install --editable .
python -m ipykernel install --user --name tcvx21
```

### Option C: install via Docker

A third way to interact with the library is to use Docker, which is a 'container' service. Dockerfiles for mybinder.org (`Dockerfile`) and a standalone docker instance (`Dockerfile.local`) are included in the repository.
To install an image for the software run
`docker build -f Dockerfile.local -t tcvx21 .`
and then launch the image with
`docker run -it --rm -p 8888:8888 -p 8787:8787 tcvx21`

This is helpful for ensuring that the software libraries are exactly preserved (for reproducibility). One note is that you
have to be careful about the memory of the image -- the notebooks will crash unless the image has at least a few gigabytes of
memory.

### To run tests and open Jupyter

Once you've installed via either option, you can activate the python environment with `conda activate ./env`.

Then, it is recommended to run the test suite with `pytest` which
ensures that everything is installed and working correctly. If something fails, let us know in the issues.

Finally, run `jupyter lab` to open a Jupyter server in the TCV-X21 repository. Then, you can open any of the notebooks (`.ipynb` extension) by clicking in the side-bar.

### A note on pinned dependencies

To ensure that the results are reproducible, the `environment.yml` file has pinned dependencies. However, if you want to use
this software as a library, pinned dependencies are unnecessarily restrictive (and can introduce security issues).
If using the software beyond reproducing the paper results, **please reinstall the library using `dev_env.yml`**.

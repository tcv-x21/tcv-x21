#!/usr/bin/env bash

# Simple command line utility to install the tcvx21 environment and install jupyter requirements
# First argument should be the environment name
# If the second argument is "clean", then the script will clean and reinstall the environment. If it is anything else (or empty)
# the existing environment will be reused

# Must be run from within the tcvx21 top-level directory (i.e. where this script is saved)
script_name=$0
script_full_path=$(dirname "$0")

echo "script_name: $script_name"
echo "full path: $script_full_path"

if [[ ${script_full_path} != "." ]];then
    echo "Setup script must be run locally (i.e. path = .). Exiting."
    exit 1
fi

echo "Loading module anaconda"
set +e
module load anaconda
set -e

start=`date +%s`
ENVIRONMENT=$1
CLEAN=$2

echo "environment name: ./$ENVIRONMENT"
if [[ ${CLEAN} == "clean" ]]; then
    echo "Cleaning ./$ENVIRONMENT (ctrl-D in 5s to cancel)"
    # sleep 5s
    echo "Continuing"
else
    echo "Will not clean"
fi

# Check if the environment already exits
if [ -d ./$ENVIRONMENT ]; then
    if [[ ${CLEAN} == "clean" ]]; then
        echo "Cleaning and rebuilding environment for ./$ENVIRONMENT"
        conda remove --prefix ./$ENVIRONMENT --all -y
        conda env create --prefix ./$ENVIRONMENT --file=environment.yml
    else
        echo "Found existing environment for ./$ENVIRONMENT. Use ./install_env.sh ./$ENVIRONMENT clean to clean install"
    fi
else
    echo "No installation found for ./$ENVIRONMENT. Building."
    conda env create --prefix ./$ENVIRONMENT --file=environment.yml
fi

# Activate the virtual environment
source activate ./$ENVIRONMENT
echo "Unloading module anaconda"
set +e
module unload anaconda
set -e

WHICH_PYTHON=$(which python)
echo "Using python=$WHICH_PYTHON"
if grep -q ./$ENVIRONMENT <<< "$WHICH_PYTHON"; then
    echo "Activated ./$ENVIRONMENT"
else
    echo "Failed to activate environment ./$ENVIRONMENT"
    exit 1
fi

# Install the tcvx21 module
echo "Installing tcvx21 module"
pip install --editable .

# Install the jupyter kernel
echo "Installing the tcvx21 Jupyter kernel"
echo "If this fails, try append the following to your ~/.bashrc and running source ~/.bashrc"
echo "export JUPYTERLAB_DIR=\"$HOME/.local/share/jupyter/lab\""
python -m ipykernel install --user --name tcv-x21

end=`date +%s`
runtime=$((end-start))
echo "Finished in $runtime s"

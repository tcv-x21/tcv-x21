# Notebooks

Jupyter notebooks showing some of the validation analysis. Notebooks are preferred to scripts here, since it is possible to include extended comments along with the source code.

If you'd like to interact with these notebooks, we recommend using the binder service (see the badge in the Gitlab repository). This allows you to interact with the notebooks via a web service.

Alternatively, you can follow the installation instructions in the README and launch a jupyter lab session via `jupyter lab` in the terminal.

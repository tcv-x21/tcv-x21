import numpy as np
import matplotlib.pyplot as plt
from tcvx21 import Quantity


def set_limits_from_observable(experimental_data,
                               field_direction, diagnostic, observable_key='jsat',
                               position_min=Quantity(-np.inf, 'cm'), position_max=Quantity(np.inf, 'cm'),
                               plot: bool=False):
    """
    Sets a mask on the experimental data to limit the range which should be included for analysis
    """

    if plot: _, ax = plt.subplots(figsize=(3,2))
    observable = experimental_data[field_direction].get_observable(diagnostic, observable_key)
    # Reset the mask to plot full range
    observable.set_mask()

    if plot:
        observable.plot(ax)
        ax.set_title(f"{field_direction}:{diagnostic}:{observable_key}")
        ax.axvline(position_min)
        ax.axvline(position_max)

    observable.set_mask(position_min=position_min, position_max=position_max)
    if plot: plt.errorbar(observable.positions, observable.values, observable.errors, color='C1')

    for diagnostic_, observable_ in experimental_data[field_direction].keys():
        if diagnostic_ == diagnostic:
            observable = experimental_data[field_direction].get_observable(diagnostic_, observable_)
            if observable.is_empty:
                continue
            else:
                observable.set_mask(position_min=position_min, position_max=position_max)
import pytest
import papermill as pm
from pathlib import Path
from tcvx21 import repository_dir, notebooks_dir, experimental_reference_dir

def execute_notebook(notebook_path: Path, tmp_path):
    """
    Executes a notebook programmatically
    """

    assert notebook_path.exists(), f"{notebook_path.absolute()} not found"

    pm.execute_notebook(notebook_path, tmp_path/'test.ipynb',
                        parameters=dict(), cwd=str(notebook_path.parent), kernel_name='tcv-x21')

def test_failing_notebook(tmp_path):
    """
    Makes sure that the pytests will fail if the notebook execution raises an error
    """

    with pytest.raises(pm.exceptions.PapermillExecutionError):
        execute_notebook(Path(__file__).parent / "failing_notebook.ipynb", tmp_path)

notebooks = dict(
    tcvx21 = repository_dir / "tcv-x21.ipynb",
    simulation_setup = notebooks_dir / "simulation_setup.ipynb",
    simulation_postprocessing = notebooks_dir / "simulation_postprocessing.ipynb",
    data_exploration = notebooks_dir / "data_exploration.ipynb",
    bulk_process = notebooks_dir / "bulk_process.ipynb",
    TCV_processing = experimental_reference_dir / "TCV_processing.ipynb",
    RDPA_coordinates = experimental_reference_dir / "reference_scenario" / "RDPA_coordinates.ipynb"
)

@pytest.fixture(params=notebooks.keys())
def notebook(request):
    """
    Fixture for many test notebooks
    """
    return notebooks[request.param]

def test_notebooks(notebook, tmp_path):
    execute_notebook(notebook, tmp_path)

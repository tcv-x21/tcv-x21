"""
Installs the Python package of processing routines
"""
from setuptools import setup, find_packages

setup(name='tcvx21', version='1.0', packages=find_packages())

# Python analysis routines for TCV-X21

This folder contains a reasonably extensive analysis library. These are the routines used for the analysis in the TCV-X21 paper, and they are included here in case you find them useful.

There is a *lot* of functionality, so unfortunately if you're not familiar with Python these might be a bit difficult to pick up. However, feel free to contact the authors for a tour.

Some interesting things you can try (i.e. in the Binder service).

## Unit conversions

```python
from tcvx21 import Quantity

my_temp = Quantity(13.5, 'eV')
kB = Quantity(1, 'boltzmann_constant')

print(f"My temperature was {my_temp}, which is {(my_temp/kB).to('kelvin')}")
```

## Plot the experimental data

```python
import tcvx21

# Load the data file
# tcvx21.experimental_reference_dir is replaced by the path tcv-x21/1.experimental_data
record = tcvx21.Record(tcvx21.experimental_reference_dir / 'TCV_forward_field.nc')

# Load the density from the Low-field-side Langmuir probe
lfs_density = record.get_observable('LFS-LP', 'density')

# Plot the LFS density
lfs_density.plot()
```

## Navigating for developers

* `record_c`: a class to wrap the standard NetCDF files
* `observable_c`: a class to interface with single observables
* `units_m.py`: an interface to Pint (unit-conversion)
* `plotting`: all of the routines need to make the figures in `results`. Note that, at the lowest level, the plot command eventually calls a `.plot` method on the observable class -- since 1D and 2D observables are handled differently.
* `grillix_post`: an extensive post-processing library for GRILLIX data, which may help if you need to write your own post-processing library.

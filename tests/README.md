# Testing for the TCV-X21 Python analysis routines

Tests are a vital part of quality assurance for any code, including post-processing scripts!

To test the python library, you can run
```bash
pytest
```
from the `tcvx21` repository.

## Notes on testing

The notebooks in the tcvx21 validation repository are tested via `papermill`. This is
provides only limited testing, to ensure that the notebooks run through without error. This is done in `test_notebooks.py`.

The other tests here can be broadly defined as either *coverage* or *functional* tests.

Coverage tests aim to hit as many lines of code as possible with typical inputs, to make sure that the code *runs*.
These are quite easy to write. A slight improvement might be to pick a result of the run and to ensure that this
doesn't change.

Functional tests are much more difficult to write. They essentially check that the computed result is equal to
an expected result. This usually requires an analytical result for a simple test. These have been written for
complex functionality like the vector analysis.

For plotting routines, the tests write sample outputs to ensure that the style is what you want. These can be found in `results/test_fig` (they will be updated each time that you run pytest).

N.b. the sample data is an old 2mm GRILLIX case.
